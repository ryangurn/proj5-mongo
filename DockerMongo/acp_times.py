"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

CLOSE_DATA = [(1000, 11.428), (600, 15), (400, 15), (200, 15), (0, 15)]
END_BREVET_CLOSE = [(1000, 4500), (600, 2400), (400, 1620), (200, 810)]
OPEN_DATA = [(1000, 28), (600, 30), (400, 32), (200, 34), (0, 34)]


#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    total_time = 0

    if control_dist_km >= brevet_dist_km:
        control_dist_km = brevet_dist_km

    for i in range(len(OPEN_DATA)):  # loop through all of the open data
        if control_dist_km >= OPEN_DATA[i][0]:  # if the control
            km = control_dist_km - OPEN_DATA[i][0]  # find distance
            time = km / OPEN_DATA[i - 1][1]  # use distance to calculate time
            total_time += time  # add to running total
            control_dist_km -= km  # update total distance so far

    # converts time decimal into correctly formatted time (hours + minutes)
    brevet_start_time = arrow.get(brevet_start_time, tzinfo='US/Pacific')  # get the correct object type
    hours = int(total_time)  # get the number of hours
    minutes = round(60 * (
                total_time - hours))  # calculate the number of minutes by taking the total time and removing all of the time for the hours
    opening_time = brevet_start_time.shift(hours=hours, minutes=minutes)  # arrow object

    return opening_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
       in kilometers, which must be one of 200, 300, 400, 600, or 1000
       (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    total_time = 0

    if control_dist_km == 0:
        br = arrow.get(brevet_start_time, tzinfo='US/Pacific')  # get the correct object type
        closing_time = br.shift(hours=+1)
        return closing_time.isoformat()
    elif control_dist_km >= brevet_dist_km:
        br1 = arrow.get(brevet_start_time, tzinfo='US/Pacific')
        key = -1
        for k, it in enumerate(END_BREVET_CLOSE):
            if it[0] == control_dist_km:
                key = k
                break
        minutes = END_BREVET_CLOSE[key][1]
        print("key({}) minutes({})".format(key, minutes))
        closing_time = br1.shift(minutes=+minutes)

        return closing_time.isoformat()

    for i in range(len(CLOSE_DATA)):
        if control_dist_km >= CLOSE_DATA[i][0]:
            km = control_dist_km - CLOSE_DATA[i][0]  # find distance
            time = km / CLOSE_DATA[i - 1][1]  # use distance to calculate time
            total_time += time  # add to running total
            control_dist_km -= km  # update total distance so far

    # converts time decimal into correctly formatted time (hours + minutes)
    brevet_start_time = arrow.get(brevet_start_time, tzinfo='US/Pacific')
    hours = int(total_time)
    minutes = round(60 * (total_time - hours))
    closing_time = brevet_start_time.shift(hours=hours, minutes=minutes)

    return closing_time.isoformat()
