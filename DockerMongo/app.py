import datetime
import os

import arrow
import pymongo
from flask import Flask, redirect, url_for, request, render_template, session, jsonify
from pymongo import MongoClient
import acp_times  # Brevet time calculations
import config  # config
import logging

###
# Globals
###
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevet


@app.route("/")
@app.route("/index")
def index():
    count = db.data.find({}).count()
    g = True

    if count <= 0:
        g = False

    app.logger.debug("Main page entry")
    return render_template('calc.html', get=g)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', 200, type=int)
    start = request.args.get('start', None, type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("dist={}".format(dist))
    app.logger.debug("start={}".format(start))
    app.logger.debug("request.args: {}".format(request.args))

    a = arrow.get(start, 'YYYY-MM-DD HH:mm', tzinfo='US/Pacific')

    open_time = acp_times.open_time(km, dist, a.isoformat())
    close_time = acp_times.close_time(km, dist, a.isoformat())
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)


###############
#
# Submit + Get Functionality for the RUSA
#   200km RUSA BREVET
# Checkpoint       Date  Time
# ==========       ====  ====
#     0km   start: 01/01 00:00
#           close: 01/01 01:00
#
#    60km    open: 01/01 01:46
#           close: 01/01 04:00
#
#   120km    open: 01/01 03:32
#           close: 01/01 08:00
#
#   175km    open: 01/01 05:09
#           close: 01/01 11:40
#
#   200km    open: 01/01 05:53
#           close: 01/01 13:30
#
###############
@app.route('/submit', methods=['POST'])
def submit():
    data = request.form
    iterator = 0
    dist = data.get('distance')
    begin_date = data.get('begin_date')
    begin_time = data.get('begin_time')

    # organize the data into an array
    miles = []
    km = []
    open = []
    close = []
    length = 0
    for v in data:
        split = v.split("_", 1)
        if split[0] != 'begin':  # strip out begin time and begin date
            if len(split) == 2:  # remove distance
                key = split[1]  # index
                value = split[0]  # miles/km/open/close
                if data["_".join(split)] != "":  # remove empty input slots
                    if value == "miles":
                        miles.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "km":
                        km.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "close":
                        close.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "open":
                        open.append(data["_".join(split)])
                        length += 1 / 4
                    print(length, key, value, "_".join(split), data["_".join(split)])
    length = int(length)

    if length == 0:
        return redirect('/')

    output = {}
    for i in range(0, length):
        output.update({str(i): {"miles": miles[i], "km": km[i], "open": open[i], "close": close[i]}})

    db.data.insert_one({'data': output, 'created': datetime.datetime.utcnow(), 'distance': dist, 'begin_date': begin_date, 'begin_time': begin_time})
    return redirect('/')


@app.route('/get')
def get():
    data = db.data.find().sort('created', pymongo.DESCENDING).limit(1)
    list = db.data.find({})
    newData = None
    for d in data:
        newData = d

    viewData = newData['data']
    distance = newData['distance']

    return render_template('get.html', distance=distance, data=viewData, list=list)


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=CONFIG.DEBUG)