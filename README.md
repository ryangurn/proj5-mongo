# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## About Me
Ryan Gurnick

rgurnick@uoregon.edu

## Tests 
##### (Mainly for Yukhe)
1. Testing Save Functionality
    1. Open up a web browser and navigate to `localhost:5000` 
    2. Select the overall brevet distance you prefer. ![Image 2](docs/img/2.png)
    3. Select a begin date and begin time ![Image 3](docs/img/3.png)
    4. Insert a km/mi value and you should see the calculated open and close times display on the right side.
    5. Repeat step 4 until you have inserted all data into the form. ![Image 45](docs/img/45.png)
    6. Click the `Save` button to the right of the distance selector. ![Image 6](docs/img/6.png)

2. Testing Get Functionality
    - **Note that after the first data record exists, the get button will just display the most recently created worksheet**
    1. Perform the steps in testing save functionality. 
    2. Select the `Get` button to the right of the `Save` button. ![Image 7](docs/img/7.png)
    3. View the data ![Image 8](docs/img/8.png)

## Functionality
* The working application. (This include the RUSA calculator functionality)
* A README.md file that includes not only identifying information (your name) but but also a revised, clear specification 
  of the brevet controle time calculation rules.
* Dockerfile
* Test cases for the two buttons. No need to run nose.
* docker-compose.yml

## RUSA calculation rules
## Non Technical Documentation
* (https://rusa.org/pages/acp-brevet-control-times-calculator)[https://rusa.org/pages/acp-brevet-control-times-calculator]
* (https://rusa.org/pages/rulesForRiders)[https://rusa.org/pages/rulesForRiders] Article 9 for Overall End Times

## Final Closing Times
1000 km = 75:00
600 km = 40:00
400 km = 27:00
300 km = 20:00
200 km = 13:30
